# edep-sim-container



## Getting started

This is small project based on how to make edep-sim container with all needed software in it. 


## Requirements

You should have Docker and VcXsrv on your device.

## Installation

1. Download and run VcXsrv.
2. Build image with Dockerfile 
You can do it in VScode (open Dockerfile in VScode, make a right-click on script or on file at VScode explorer, and press "Build image")
Or you can do it from cmd 
```
docker build --pull --rm -f "Dockerfile" -t edep-sim-container:latest "." 
```
3. After finishing of the instalation you will see new window of pcmanfm explorer. You can continue on work in it (cmd can be called pressing f4 bottom or you can find it in "Tools"->"open current folder in terminal") or open started image in VScode (load docker extantion, find started image and press "Attach shell" or "Attach VScode")

